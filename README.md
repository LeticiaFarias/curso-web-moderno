# Web Moderno

* [ ] Fundamentos de JavaScript
* [ ] Estruturas de Controle
* [ ] Função
* [ ] Objeto
* [ ] Array
* [ ] Node
* [ ] ESNext
* [ ] Conceitos sobre Web
* [ ] HTML
* [ ] CSS
* [ ] Integrando HTML, CSS e JS
* [ ] Ajax
* [ ] Gulp
* [ ] Webpack
* [ ] jQuery
* [ ] Bootstrap
* [ ] React
* [ ] VueJS
* [ ] Banco Relacional
* [ ] Banco não-relacional
* [ ] Express
* [ ] Projeto Backend
* [ ] Projeto Frontend

